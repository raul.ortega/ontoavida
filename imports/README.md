## Select terms from existing external ontologies.

---

### write down (as files) the IRIs of the terms to be imported:
> *FBcv_imports.txt*

> *gsso_imports.txt*

> *ncit-imports.txt*

> *ro_imports.txt*

> *stato_imports.txt*

### download the latest versions of the external ontologies:
> *FlyBase Controlled Vocabulary (FBcv)*

curl -L http://purl.obolibrary.org/obo/FBcv.owl > external_ontologies/FBcv.owl

> *Gender, Sex, and Sexual Orientation (GSSO) ontology*

curl -L http://purl.obolibrary.org/obo/gsso.owl > external_ontologies/gsso.owl

> *NCI Thesaurus OBO Edition*

curl -L http://purl.obolibrary.org/obo/ncit.owl > external_ontologies/ncit.owl

> *Relation Ontology*

curl -L http://purl.obolibrary.org/obo/ro.owl > external_ontologies/ro.owl

> *The Statistical Methods Ontology*

curl -L http://purl.obolibrary.org/obo/stato.owl > external_ontologies/stato.owl

### extract ontology modules from the external ontologies using ROBOT:
```
robot extract \
    --method STAR \
    --input external_ontologies/FBcv.owl \
    --term-file FBcv_imports.txt \
    --copy-ontology-annotations true \
    --intermediates none \
    --imports exclude \
    --output FBcv_imports.owl
```

```
robot extract \
    --method STAR \
    --input external_ontologies/ncit.owl \
    --term-file ncit_imports.txt \
    --copy-ontology-annotations true \
    --intermediates none \
    --imports exclude \
    --output ncit_imports.owl
```

```
robot extract \
    --method STAR \
    --input external_ontologies/ogg.owl \
    --term-file ogg_imports.txt \
    --copy-ontology-annotations true \
    --intermediates none \
    --imports exclude \
    --output ogg_imports.owl
```

```
robot extract \
    --method STAR \
    --input external_ontologies/ro.owl \
    --term-file ro_imports.txt \
    --copy-ontology-annotations true \
    --intermediates none \
    --imports exclude \
    --output ro_imports.owl
```

```
robot extract \
    --method STAR \
    --input external_ontologies/stato.owl \
    --term-file stato_imports.txt \
    --copy-ontology-annotations true \
    --intermediates none \
    --imports exclude \
    --output stato_imports.owl
```

### remove unwanted terms and annotations from the imported modules using ROBOT:
> some terms are imported without annotations so that we can use ours:

```
robot filter \
    --input FBcv_imports.owl \
    --term FBcv:0000212 \
    --select annotations \
    --signature true \
    --output FBcv_imports-annotated.owl
```

```
robot remove \
    --input gsso_imports.owl \
    --exclude-term GSSO:009994 \
    --output gsso_imports-unannotated.owl
```

```
robot remove \
    --input ncit_imports.owl \
    --exclude-term NCIT:C42790 \
    --output ncit_imports-unannotated.owl
```

```
robot filter \
    --input /ro_imports.owl \
    --term RO:0002443 \
    --select "annotations self descendants" \
    --select annotations \
    --signature true \
    --output ro_imports-annotated.owl
```

```
robot remove \
    --input ro_imports.owl \
    --exclude-term RO:0000056 \
    --exclude-term RO:0002331 \
    --exclude-term RO:0002354 \
    --exclude-term RO:0002353 \
    --exclude-term RO:0002507 \
    --exclude-term RO:0002180 \
    --output ro_imports-unannotated.owl
```

```
robot remove \
    --input stato_imports.owl \
    --exclude-term STATO:0000002 \
    --output stato_imports-unannotated.owl
```

---
