# OntoAvida: ontology for the Avida digital evolution platform.
> workflow for a new release.

---

## 1. Select terms from existing reference ontologies.
### 2.1. write down (as files) the IRIs of the terms to be imported from external ontologies:
> *imports/FBcv_imports.txt*

> *imports/gsso_imports.txt*

> *imports/ncit-imports.txt*

> *imports/ro_imports.txt*

> *imports/stato_imports.txt*

### 1.2. download the latest versions of the external ontologies:
> *FlyBase Controlled Vocabulary (FBcv)*

curl -L http://purl.obolibrary.org/obo/FBcv.owl > imports/external_ontologies/FBcv.owl

> *Gender, Sex, and Sexual Orientation (GSSO) ontology*

curl -L http://purl.obolibrary.org/obo/gsso.owl > imports/external_ontologies/gsso.owl

> *NCI Thesaurus OBO Edition*

curl -L http://purl.obolibrary.org/obo/ncit.owl > imports/external_ontologies/ncit.owl

> *Relation Ontology*

curl -L http://purl.obolibrary.org/obo/ro.owl > imports/external_ontologies/ro.owl

> *The Statistical Methods Ontology*

curl -L http://purl.obolibrary.org/obo/stato.owl > imports/external_ontologies/stato.owl

### 1.3. extract ontology modules from the external ontologies:
```
robot extract \
    --method STAR \
    --input imports/external_ontologies/FBcv.owl \
    --term-file imports/FBcv_imports.txt \
    --copy-ontology-annotations true \
    --intermediates none \
    --imports exclude \
    --output imports/FBcv_imports.owl
```

```
robot extract \
    --method STAR \
    --input imports/external_ontologies/gsso.owl \
    --term-file imports/gsso_imports.txt \
    --copy-ontology-annotations true \
    --intermediates none \
    --imports exclude \
    --output imports/gsso_imports.owl
```

```
robot extract \
    --method STAR \
    --input imports/external_ontologies/ncit.owl \
    --term-file imports/ncit_imports.txt \
    --copy-ontology-annotations true \
    --intermediates none \
    --imports exclude \
    --output imports/ncit_imports.owl
```

```
robot extract \
    --method STAR \
    --input imports/external_ontologies/ro.owl \
    --term-file imports/ro_imports.txt \
    --copy-ontology-annotations true \
    --intermediates none \
    --imports exclude \
    --output imports/ro_imports.owl
```

```
robot extract \
    --method STAR \
    --input imports/external_ontologies/stato.owl \
    --term-file imports/stato_imports.txt \
    --copy-ontology-annotations true \
    --intermediates none \
    --imports exclude \
    --output imports/stato_imports.owl
```

### 1.4. performing a quality control check on the imported modules:
```
robot report \
    --input imports/FBcv_imports.owl \
    --output reports/quality_control_check_imports_FBcv.tsv
```

```
robot report \
    --input imports/gsso_imports.owl \
    --output reports/quality_control_check_imports_gsso.tsv
```

```
robot report \
    --input imports/ncit_imports.owl \
    --output reports/quality_control_check_imports_ncit.tsv
```

```
robot report \
    --input imports/ro_imports.owl \
    --output reports/quality_control_check_imports_ro.tsv
```

```
robot report \
    --input imports/stato_imports.owl \
    --output reports/quality_control_check_imports_stato.tsv
```

### 1.5. remove unwanted terms and annotations from the imported modules:
> some terms are imported without annotations so that we can use ours:

```
robot filter \
    --input imports/FBcv_imports.owl \
    --term FBcv:0000212 \
    --select annotations \
    --signature true \
    --output imports/FBcv_imports-annotated.owl
```

```
robot remove \
    --input imports/gsso_imports.owl \
    --exclude-term GSSO:009994 \
    --output imports/gsso_imports-unannotated.owl
```

```
robot remove \
    --input imports/ncit_imports.owl \
    --exclude-term NCIT:C42790 \
    --output imports/ncit_imports-unannotated.owl
```

```
robot filter \
    --input imports/ro_imports.owl \
    --term RO:0002443 \
    --select "annotations self descendants" \
    --select annotations \
    --signature true \
    --output imports/ro_imports-annotated.owl
```

```
robot remove \
    --input imports/ro_imports.owl \
    --exclude-term RO:0000056 \
    --exclude-term RO:0002331 \
    --exclude-term RO:0002354 \
    --exclude-term RO:0002353 \
    --exclude-term RO:0002507 \
    --exclude-term RO:0002180 \
    --output imports/ro_imports-unannotated.owl
```

```
robot remove \
    --input imports/stato_imports.owl \
    --exclude-term STATO:0000002 \
    --output imports/stato_imports-unannotated.owl
```

### 1.6. perform a quality control check on the imported modules:
```
robot report \
    --input imports/FBcv_imports-annotated.owl \
    --output reports/quality_control_check_imports_FBcv-annotated.tsv
```

```
robot report \
    --input imports/gsso_imports-unannotated.owl \
    --output reports/quality_control_check_imports_gsso-unannotated.tsv
```

```
robot report \
    --input imports/ncit_imports-unannotated.owl \
    --output reports/quality_control_check_imports_ncit-unannotated.tsv
```

```
robot report \
    --input imports/ro_imports-annotated.owl \
    --output reports/quality_control_check_imports_ro-annotated.tsv
```

```
robot report \
    --input imports/ro_imports-annotated.owl \
    --output reports/quality_control_check_imports_ro-unannotated.tsv
```

```
robot report \
    --input imports/stato_imports-unannotated.owl \
    --output reports/quality_control_check_imports_stato-unannotated.tsv
```

---

## 2. Contributors can propose new terms by adding them in the following template files.
> *templates/class_template.tsv*

> *templates/object_property_template.tsv*

> *templates/datatype_property_template.tsv*

### 2.1. convert template files into owl modules:
```
robot template \
    --template templates/class_template.tsv \
    --prefix "FBcv: http://purl.obolibrary.org/obo/FBcv_" \
    --prefix "ONTOAVIDA: http://purl.obolibrary.org/obo/ONTOAVIDA_" \
    --output modules/class_module.owl
```

```
robot template \
    --template templates/object_property_template.tsv \
    --prefix "FBcv: http://purl.obolibrary.org/obo/FBcv_" \
    --prefix "GSSO: http://purl.obolibrary.org/obo/GSSO_" \
    --prefix "NCIT: http://purl.obolibrary.org/obo/NCIT_" \
    --prefix "ONTOAVIDA: http://purl.obolibrary.org/obo/ONTOAVIDA_" \
    --prefix "RO: http://purl.obolibrary.org/obo/RO_" \
    --prefix "STATO: http://purl.obolibrary.org/obo/STATO_" \
    --output modules/object_property_module.owl
```

```
robot template \
    --template templates/datatype_property_template.tsv \
    --prefix "FBcv: http://purl.obolibrary.org/obo/FBcv_" \
    --prefix "GSSO: http://purl.obolibrary.org/obo/GSSO_" \
    --prefix "ONTOAVIDA: http://purl.obolibrary.org/obo/ONTOAVIDA_" \
    --output modules/datatype_property_module.owl
```

### 2.2. annotate the template modules:
```
robot annotate \
    --input modules/class_module.owl \
    --annotation http://purl.org/dc/elements/1.1/title "Ontology module from the class template" \
    --annotation http://purl.org/dc/elements/1.1/description "Ontology module to be added to the ontoavida-edit ontology" \
    --annotation http://purl.org/dc/terms/license "OntoAvida by https://fortunalab.org is licensed under CC BY 4.0." \
    --ontology-iri "https://gitlab.com/fortunalab/ontoavida/modules/class_module.owl-annotated.owl" \
    --output modules/class_module-annotated.owl
```

```
robot annotate \
    --input modules/object_property_module.owl \
    --annotation http://purl.org/dc/elements/1.1/title "Ontology module from the object_property template" \
    --annotation http://purl.org/dc/elements/1.1/description "Ontology module to be added to the ontoavida-edit ontology" \
    --annotation http://purl.org/dc/terms/license "OntoAvida by https://fortunalab.org is licensed under CC BY 4.0." \
    --ontology-iri "https://gitlab.com/fortunalab/ontoavida/modules/object_property_module.owl-annotated.owl" \
    --output modules/object_property_module-annotated.owl
```

```
robot annotate \
    --input modules/datatype_property_module.owl \
    --annotation http://purl.org/dc/elements/1.1/title "Ontology module from the datatype_property template" \
    --annotation http://purl.org/dc/elements/1.1/description "Ontology module to be added to the ontoavida-edit ontology" \
    --annotation http://purl.org/dc/terms/license "OntoAvida by https://fortunalab.org is licensed under CC BY 4.0." \
    --ontology-iri "https://gitlab.com/fortunalab/ontoavida/modules/datatype_property_module.owl-annotated.owl" \
    --output modules/datatype_property_module-annotated.owl
```

### 2.3. perform a quality control check on the annotated template modules:
```
robot report \
    --input modules/class_module-annotated.owl \
    --output reports/quality_control_check_class_module-annotated.tsv
```

```
robot report \
    --input modules/object_property_module-annotated.owl \
    --output reports/quality_control_check_object_property_module-annotated.tsv
```

```
robot report \
    --input modules/datatype_property_module-annotated.owl \
    --output reports/quality_control_check_datatype_property_module-annotated.tsv
```

---

## 3. Merge imported modules and template modules with the core ontology.

### 3.1. perform a quality control check on the ontoavida-edit ontology (file edited with Protege):
```
robot report \
    --input ontoavida-edit.owl \
    --output reports/quality_control_check_ontoavida-edit.tsv
```

### 3.2. merge modules with the ontoavida-edit file:
```
robot merge \
    --input ontoavida-edit.owl \
    --input imports/FBcv_imports-annotated.owl \
    --input imports/gsso_imports-unannotated.owl \
    --input imports/ncit_imports-unannotated.owl \
    --input imports/ro_imports-annotated.owl \
    --input imports/ro_imports-unannotated.owl \
    --input imports/stato_imports-unannotated.owl \
    --input modules/class_module-annotated.owl \
    --input modules/object_property_module-annotated.owl \
    --input modules/datatype_property_module-annotated.owl \
    --output results/ontoavida-merged.owl
```

### 3.3. perform a quality control check on the recently merged ontology:
```
robot report \
    --input results/ontoavida-merged.owl \
    --output reports/quality_control_check_ontoavida-merged.tsv
```

---

## 4. Check the logical consistency of the ontology and perform automated classification of terms.

### 4.1. ontology reasoning:
```
robot reason --reasoner ELK \
    --input results/ontoavida-merged.owl \
    --output results/ontoavida-reasoned.owl
```

### 4.2. relax equivalent axioms:
```
robot relax \
    --input results/ontoavida-reasoned.owl \
    --output results/ontoavida-relaxed.owl
``` 

### 4.3. remove redundant axioms:
```
robot reduce \
    --reasoner ELK \
    --input results/ontoavida-relaxed.owl \
    --output results/ontoavida-reduced.owl
```

### 4.4. update annotations before releasing (e.g., dated version IRI):

```
robot annotate \
    --input results/ontoavida-reduced.owl \
    --version-iri "http://purl.obolibrary.org/obo/ontoavida/2021-09-01/ontoavida.owl" \
    --output results/ontoavida-annotated.owl
```

### 4.5. OWL-DL profile validation:
```
robot validate-profile --profile DL \
  --input results/ontoavida-annotated.owl \
  --output reports/ontoavida-annotated_validation.tsv
```

### 4.6. quality control SPARQL queries:
```
robot report \
    --input results/ontoavida-annotated.owl \
    --output reports/ontoavida-annotated_report.tsv
```

---

## 5. New release.

### 5.1. create a list of terms (i.e., reporting the new terms added to the ontology):
```
robot verify \
    --input results/ontoavida-annotated.owl \
    --queries sparql/terms.sparql \
    --output-dir reports/
```

### 5.2. create the latest version of the ontology as an OBO file:
```
robot convert \
    --input results/ontoavida-annotated.owl \
    --output results/ontoavida-annotated.obo
```

### 5.3. copy the latest version of the ontology as OWL and OBO files into the root folder and the *releases* folder:
```
cp results/ontoavida-annotated.owl ontoavida.owl

cp results/ontoavida-annotated.obo ontoavida.obo

cp results/ontoavida-annotated.owl releases/YYYY-MM-DD.owl

cp results/ontoavida-annotated.obo releases/YYYY-MM-DD.obo
```

---